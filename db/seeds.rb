# frozen_string_literal: true

# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

# rubocop:disable Layout/LineLength
stops = [{ atco_code: '1200POA10345', common_name: 'Deverel Farm Turning', latitude: 50.7832160951, longitude: -2.2658418511 },
         { atco_code: '680005008111', common_name: 'Brooms Road', latitude: 55.0685981948, longitude: -3.5977395048 },
         { atco_code: '3800C913501', common_name: 'Silica Road', latitude: 52.6259997814, longitude: -1.6498646729 },
         { atco_code: '4400LH0469', common_name: 'Essex Road', latitude: 50.7928292409, longitude: -0.6819176823 },
         { atco_code: '9100LNDNBDC', common_name: 'London Bridge Rail Station', latitude: 51.5050190405, longitude: -0.0860920048 }]
# rubocop:enable Layout/LineLength
stops.each { |stop| Stop.create(stop) }
