# frozen_string_literal: true

# Create Public Transport Stops
class CreateStops < ActiveRecord::Migration[6.0]
  def change
    create_table :stops do |t|
      t.string :atco_code
      t.string :common_name
      t.float :latitude
      t.float :longitude
    end
  end
end
