# frozen_string_literal: true

require 'csv'

namespace :db do
  task setup: :environment do
  	Rake::Task['db:create'].invoke
  	Rake::Task['db:migrate'].invoke
  	Rake::Task['db:load:stops'].invoke
  	Rake::Task['db:seed'].invoke
  end

  namespace :load do
    desc 'Load CSV file provided by NaPTAN containing all public transport stops in the UK'
    task stops: :environment do
    	puts "Loading CSV..."
      file = File.read(Rails.root.join('db', 'data', 'stops.csv'), encoding: 'ISO-8859-1')
      csv = CSV.parse(file, headers: true)
      csv.each do |row|
      	print '.'
        Stop.create({
                      atco_code: row['ATCOCode'],
                      common_name: row['CommonName'],
                      latitude: row['Latitude'],
                      longitude: row['Longitude']
                    })
      end
    end
  end
end
