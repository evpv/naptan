# README

This Rails App imports stops from a CSV file provided by NaPTAN and exposes a JSON search API endpoint for public transport stops in a given area, represented by a bounding box consisting of a pair of (min-max) latitude, longitude coordinates.

## Setup
- ruby version: 2.6.5
- rails version: 6.0.3.2
- `bundle install`
- `bundle exec rake db:setup`

## Run
- `bundle exec rails s`

## API
- Endpoint: `/query`
- Content Type: `application/json`
- Method: POST
- Body: `{"locations": [{"long": 0.0, "lat": 0.0}, {"long": 0.0, "lat": 0.0}]}`

## Example usage
`curl -X POST -H "Content-Type: application/json" -d '{"locations":[{"long":"-0.489","lat":"51.28"},{"long":"0.236","lat":"51.686"}]}' 'http://localhost:3000/query'`

## Spec
`bundle exec rspec spec`
