# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Stop do
  describe '#find_in_bbox' do
    let(:greater_london) do
      BoundingBox.new(Location.new({ long: -0.489, lat: 51.28 }),
                      Location.new({ long: 0.236, lat: 51.686 }))
    end

    specify do
      expect(described_class.within(greater_london).first.common_name).to eq('London Bridge Rail Station')
    end
  end
end
