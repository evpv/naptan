# frozen_string_literal: true

require 'rails_helper'

RSpec.describe BoundingBox do
  let(:bbox) do
    described_class.new(Location.new({ long: -0.489, lat: 51.28 }),
                        Location.new({ long: 0.236, lat: 51.686 }))
  end
  describe 'has a center (lat, long pair)' do
    specify do
      expect(bbox.center.long).to eq(-0.1265)
      expect(bbox.center.lat).to eq(51.483000000000004)
    end
  end

  describe 'sorts minimum and maximum longitude and latitude' do
    specify do
      expect(bbox.lat.min).to eq(51.28)
      expect(bbox.lat.max).to eq(51.686)
      expect(bbox.long.min).to eq(-0.489)
      expect(bbox.long.max).to eq(0.236)
    end
  end
end
