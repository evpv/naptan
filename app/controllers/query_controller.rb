# frozen_string_literal: true

# Used to query Stops
class QueryController < ApplicationController
  # rubocop:disable Metrics/AbcSize
  def stop
    permitted = params.require(:query).permit(locations: %i[long lat])
    locations = permitted[:locations].map { |l| Location.new(lat: l[:lat], long: l[:long]) }
    bbox = BoundingBox.new(locations.first, locations.last)
    render json: Stop.within(bbox).sort_by { |s| s.location.distance_to(bbox.center) }.first(20).to_json
  end
  # rubocop:enable Metrics/AbcSize
end
