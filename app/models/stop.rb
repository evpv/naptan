# frozen_string_literal: true

# Represents Public Transport Stop
class Stop < ApplicationRecord
  def self.within(bbox)
    where(
      "latitude >= ? AND latitude <= ? AND \
			longitude >= ? AND longitude <= ?",
      bbox.lat.min,
      bbox.lat.max,
      bbox.long.min,
      bbox.long.max
    )
  end

  def location
    Location.new(lat: latitude, long: longitude)
  end
end
