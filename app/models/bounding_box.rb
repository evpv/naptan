# frozen_string_literal: true

# Representation of area. Two locations form the diagonal of the box
class BoundingBox
  def initialize(loc1, loc2)
    @nodes = [loc1, loc2]
  end

  def center
    Location.new(lat: lat.inject(:+) / 2, long: long.inject(:+) / 2)
  end

  def lat
    @nodes.map(&:lat).map(&:to_f)
  end

  def long
    @nodes.map(&:long).map(&:to_f)
  end
end
