# frozen_string_literal: true

# Simple representation of Lat/Long pair
class Location
  attr_reader :lat, :long

  def initialize(lat:, long:)
    @lat = lat
    @long = long
  end

  def distance_to(location)
    (@lat - location.lat).abs + (@long - location.long).abs
  end
end
